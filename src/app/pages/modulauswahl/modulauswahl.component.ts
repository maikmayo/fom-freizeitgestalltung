import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-modulauswahl',
  templateUrl: './modulauswahl.component.html',
  styleUrls: ['./modulauswahl.component.scss'],
})
export class ModulauswahlComponent {
  public module = [
    { label: 'Chatten', icon: 'comments', route: 'chat', messageAmount: '5' },
    {
      label: 'Interessante Orte',
      icon: 'map',
      route: 'maps',
      messageAmount: '',
    },
    {
      label: 'Lerngruppen finden',
      disabled: true,
      icon: 'users',
      route: '',
      messageAmount: '',
    },
    {
      label: 'Events',
      disabled: true,
      icon: 'ticket',
      route: '',
      messageAmount: '',
    },
    {
      label: 'Uni-Infos',
      disabled: true,
      icon: 'info',
      route: '',
      messageAmount: '',
    },
    {
      label: 'Kalender',
      disabled: true,
      icon: 'calendar',
      route: '',
      messageAmount: '',
    },
    {
      label: 'Vernetzen',
      disabled: true,
      icon: 'sitemap',
      route: '',

      messageAmount: '',
    },
    {
      label: 'Hilfe',
      disabled: true,
      icon: 'question-circle',
      route: '',
      messageAmount: '',
    },
    {
      label: 'Einstellungen',
      disabled: true,
      icon: 'cog',
      route: '',
      messageAmount: '',
    },
  ];

  constructor(public router: Router) {}
}
