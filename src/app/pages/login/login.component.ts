import { Component } from '@angular/core';
import {
  FirebaseUISignInFailure,
  FirebaseUISignInSuccessWithAuthResult,
} from 'firebaseui-angular';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent {
  public alreadyLoggedIn = false;

  constructor(private router: Router) {}

  public errorCallback(errorData: FirebaseUISignInFailure) {
    console.log('errorCallback', errorData);
  }

  public uiShownCallback() {
    console.log('uiShownCallback');
  }

  public successCallback(
    signInSuccessData: FirebaseUISignInSuccessWithAuthResult
  ) {
    console.log('successCallback', signInSuccessData);
    this.navigateToModulauswahl();
  }

  private navigateToModulauswahl(): void {
    console.log('navigateToModulauswahl');

    this.router.navigate(['/modulauswahl']);
  }
}
