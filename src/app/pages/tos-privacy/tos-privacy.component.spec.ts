import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TosPrivacyComponent } from './tos-privacy.component';

describe('TosPrivacyComponent', () => {
  let component: TosPrivacyComponent;
  let fixture: ComponentFixture<TosPrivacyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TosPrivacyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TosPrivacyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
