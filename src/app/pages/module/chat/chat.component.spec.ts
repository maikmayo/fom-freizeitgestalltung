import { ChatService } from './../../../services/chat.service';
import { AppModule } from './../../../app.module';
import { ButtonModule } from 'primeng/button';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChatComponent } from './chat.component';
import { InputTextModule } from 'primeng/inputtext';
import { By } from '@angular/platform-browser';

describe('ChatComponent', () => {
  let component: ChatComponent;
  let fixture: ComponentFixture<ChatComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ChatComponent],
      imports: [ButtonModule, InputTextModule, AppModule],
      providers: [],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display chat messages', () => {
    let messages = fixture.debugElement.queryAll(By.css('.chat-message'));
    expect(messages.length).toEqual(0);

    component.chatMessages = [
      { user: 'test1', message: 'testMessage1', time: new Date() },
      { user: 'test2', message: 'testMessage2', time: new Date() },
      { user: 'test3', message: 'testMessage3', time: new Date() },
    ];
    fixture.detectChanges();
    messages = fixture.debugElement.queryAll(By.css('.chat-message'));
    expect(messages.length).toEqual(component.chatMessages.length);
  });

  it('should trigger sendMessage when clicking submit-button', () => {
    const sendMessageSpy = spyOn(component, 'sendMessage');
    const submitButton = fixture.debugElement.query(
      By.css('button.p-button-icon-only')
    );
    submitButton.nativeElement.click();
    expect(sendMessageSpy).toHaveBeenCalled();
  });
});
