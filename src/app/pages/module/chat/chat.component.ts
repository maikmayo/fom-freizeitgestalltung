import { ChatService } from './../../../services/chat.service';
import { Component, ElementRef, OnInit, Query, ViewChild } from '@angular/core';

@Component({
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss'],
})
export class ChatComponent implements OnInit {
  @ViewChild('chatWrapper') public chatContainer!: ElementRef;

  public chatMessages: ChatMessage[] = [];

  public newChatMessage = '';

  constructor(private chatService: ChatService) {}

  public ngOnInit(): void {
    this.chatService.getAllMessages().subscribe((dirtyMessages) => {
      this.chatMessages = [];
      dirtyMessages.forEach((dirtyMessage) => {
        const chatMessage: ChatMessage = {
          user: dirtyMessage['user'],
          message: dirtyMessage['message'],
          photoURL: dirtyMessage['photoURL'],
          time: new Date(dirtyMessage['time']['seconds'] * 1000),
        };
        this.chatMessages.push(chatMessage);
        this.chatMessages.sort((a, b) => a.time.getTime() - b.time.getTime());
      });
      this.scrollToBottom();
    });
  }

  public sendMessage(): void {
    this.chatService
      .postMessage(this.newChatMessage)
      .then(() => (this.newChatMessage = ''));
  }

  public sendMessageIfEnter(event: KeyboardEvent): void {
    if (event.code === 'Enter') {
      this.sendMessage();
    }
  }

  private scrollToBottom(): void {
    setTimeout(() => {
      this.chatContainer.nativeElement.scrollTop =
        this.chatContainer.nativeElement.scrollHeight;
    }, 0);
  }
}

interface ChatMessage {
  user: string;
  message: string;
  time: Date;
  photoURL?: string;
}
