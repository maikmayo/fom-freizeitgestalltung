import { Component } from '@angular/core';
declare var google: any;

@Component({
  templateUrl: './maps.component.html',
  styleUrls: ['./maps.component.scss'],
})
export class MapsComponent {
  public options = {
    center: { lat: 51.254090947248784, lng: 7.14019380483122 },
    zoom: 17,
  };

  public markers = [
    new google.maps.Marker({
      position: { lat: 51.25269433723041, lng: 7.132359975642232 },
      title: 'FOM Wuppertal',
    }),
    new google.maps.Marker({
      position: { lat: 51.25649438100236, lng: 7.140776552892875 },
      title: 'The Loft',
    }),
    new google.maps.Marker({
      position: { lat: 51.254869739726495, lng: 7.139172323431159 },
      title: 'Burger King',
    }),
    new google.maps.Polygon({
      paths: [
        { lat: 51.25638395991542, lng: 7.137136362821309 },
        { lat: 51.257261509678294, lng: 7.1395229159390885 },
        { lat: 51.25634909003535, lng: 7.140358673823506 },
        { lat: 51.25548895812876, lng: 7.138027837898022 },
      ],
      strokeOpacity: 0.5,
      strokeWeight: 1,
      fillColor: '#1976D2',
      fillOpacity: 0.35,
    }),
    new google.maps.Circle({
      center: { lat: 51.25470402841852, lng: 7.133391907632874 },
      fillColor: '#1976D2',
      fillOpacity: 0.35,
      strokeWeight: 1,
      radius: 100,
      title: 'Deweerthscher Garten',
    }),
  ];

  public handleMapClick(event: any): void {
    console.log(event);

    //event: MouseEvent of Google Maps api
  }

  public handleOverlayClick(event: any): void {
    console.log(event);
    //event.originalEvent: MouseEvent of Google Maps api
    //event.overlay: Clicked overlay
    //event.map: Map instance
  }
}
