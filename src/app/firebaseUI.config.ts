import { firebase, firebaseui } from 'firebaseui-angular';

export const firebaseUiAuthConfig: firebaseui.auth.Config = {
  signInFlow: 'popup',
  signInOptions: [
    firebase.auth.GoogleAuthProvider.PROVIDER_ID,
    {
      requireDisplayName: false,
      provider: firebase.auth.EmailAuthProvider.PROVIDER_ID,
    },
  ],
  tosUrl: 'tos-privacy',
  privacyPolicyUrl: 'tos-privacy',
  credentialHelper: firebaseui.auth.CredentialHelper.GOOGLE_YOLO,
};
