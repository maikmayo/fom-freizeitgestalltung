import { Injectable } from '@angular/core';
import {
  Firestore,
  collectionData,
  collection,
  DocumentData,
  CollectionReference,
} from '@angular/fire/firestore';
import {
  AngularFirestore,
  DocumentReference,
} from '@angular/fire/compat/firestore';
import { Observable } from 'rxjs';
import { AuthenticationService } from './authentication.service';

@Injectable({
  providedIn: 'root',
})
export class ChatService {
  private chatCollection: CollectionReference<DocumentData>;

  constructor(
    private firestore: Firestore,
    private afs: AngularFirestore,
    private authenticationService: AuthenticationService
  ) {
    this.chatCollection = collection(this.firestore, 'Chat');
  }

  public getAllMessages(): Observable<DocumentData[]> {
    return collectionData(this.chatCollection);
  }

  public postMessage(message: string): Promise<DocumentReference<unknown>> {
    const data = {
      message,
      user:
        this.authenticationService.currentUser?.displayName ??
        this.authenticationService.currentUser?.email,
      photoURL: this.authenticationService.currentUser?.photoURL,
      time: { seconds: Math.floor(new Date().getTime() / 1000) },
    };
    return this.afs.collection('Chat').add(data);
  }
}
